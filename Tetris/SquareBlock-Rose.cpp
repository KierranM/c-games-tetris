#include "stdafx.h"
#include "SquareBlock.h"


SquareBlock::SquareBlock(int startNRows, int startNCols, int startY, Color startColor, Grid^ startDrawGrid)
	:Block(startNRows, startNCols, startColor, startDrawGrid)
{
	//Set up the points
	SetUpPoints(startY);
}

void SquareBlock::SetUpPoints(int startY)
{
	//Sets up the points in a square shape
	int leftX = nCols/TWO;
	cellCoords[0] = gcnew Point(leftX, startY);
	cellCoords[1] = gcnew Point(leftX + 1, startY);
	startY++;
	cellCoords[TWO] = gcnew Point(leftX, startY);
	cellCoords[THREE] = gcnew Point(leftX + 1, startY);
}