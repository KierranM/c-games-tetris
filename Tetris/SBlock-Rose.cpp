#include "stdafx.h"
#include "SBlock.h"


SBlock::SBlock(int startNRows, int startNCols, int startY, Color startColor, Grid^ startDrawGrid)
	:Block(startNRows, startNCols, startColor, startDrawGrid)
{
	//Set up the points
	SetUpPoints(startY);
}

void SBlock::Rotate()
{
	//Rotates the block into one of its two orientations (North or East)

	//Store the current orientation
	Orientation newOrientation = orientation;
	//Get a copy of the cellCoords array
	array<Point^>^ temp = GetTempArray();
	switch (newOrientation)
	{
		//Moving North to East
	case Orientation::NORTH:
		temp[0]->X++;
		temp[0]->Y--;
		temp[TWO]->X++;
		temp[TWO]->Y++;
		temp[THREE]->Y += TWO;
		newOrientation = Orientation::EAST;
		break;
		//Moving East back to North
	case Orientation::EAST:
		temp[0]->X--;
		temp[0]->Y++;
		temp[TWO]->X--;
		temp[TWO]->Y--;
		temp[THREE]->Y -= TWO;
		newOrientation = Orientation::NORTH;
		break;
	}

	//Check if the rotation is valid
	if (CheckCanMove(temp))
	{
		//Change cellCoords to point at the temp array
		cellCoords = temp;
		//Update the blocks orientation
		orientation = newOrientation;
	}
}

void SBlock::SetUpPoints(int startY)
{
	//Sets up the blocks points into there appropriate positions

	//Get the middle value of the number of columns
	int startX = nCols/TWO;

	//Set up the points to make a S
	cellCoords[0] = gcnew Point(startX - 1, startY);
	cellCoords[1] = gcnew Point(startX, startY); //Center point
	cellCoords[TWO] = gcnew Point(startX, startY - 1); 
	cellCoords[THREE] = gcnew Point(startX + 1, startY - 1);
}