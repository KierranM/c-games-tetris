#pragma once
#include "TetrisGame.h"

namespace Tetris {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Panel^  gamePanel;
	protected: 

	private: System::Windows::Forms::Panel^  previewPanel;
	private: System::Windows::Forms::Button^  startButton;
	private: System::Windows::Forms::Label^  scoreLabel;
	private: System::Windows::Forms::Label^  levelLabel;
	private: System::ComponentModel::IContainer^  components;
	private: System::Windows::Forms::Timer^  gameTimer;
	private: System::Windows::Forms::Label^  scoreCount;
	private: System::Windows::Forms::Label^  levelCount;
	private: System::Windows::Forms::Timer^  drawTimer;
	private: System::Windows::Forms::Label^  gameOver;
	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


	private:
		TetrisGame^ game;
		Random^ rand;
		Bitmap^ gridImage;
		Bitmap^ previewImage;
		Bitmap^ cellImage;
		Graphics^ gridBuffer;
		Graphics^ previewBuffer;
	    Graphics^ panelGraphics;
		Graphics^ previewPanelGraphics;
		Graphics^ formGraphics;
	private: System::Windows::Forms::PictureBox^  pictureBox1;
			 bool bordersPainted;


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Form1::typeid));
			this->gamePanel = (gcnew System::Windows::Forms::Panel());
			this->previewPanel = (gcnew System::Windows::Forms::Panel());
			this->startButton = (gcnew System::Windows::Forms::Button());
			this->scoreLabel = (gcnew System::Windows::Forms::Label());
			this->levelLabel = (gcnew System::Windows::Forms::Label());
			this->gameTimer = (gcnew System::Windows::Forms::Timer(this->components));
			this->scoreCount = (gcnew System::Windows::Forms::Label());
			this->levelCount = (gcnew System::Windows::Forms::Label());
			this->drawTimer = (gcnew System::Windows::Forms::Timer(this->components));
			this->gameOver = (gcnew System::Windows::Forms::Label());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox1))->BeginInit();
			this->SuspendLayout();
			// 
			// gamePanel
			// 
			this->gamePanel->BackColor = System::Drawing::Color::Black;
			this->gamePanel->Location = System::Drawing::Point(297, 59);
			this->gamePanel->Name = L"gamePanel";
			this->gamePanel->Size = System::Drawing::Size(375, 705);
			this->gamePanel->TabIndex = 0;
			// 
			// previewPanel
			// 
			this->previewPanel->BackColor = System::Drawing::Color::Black;
			this->previewPanel->Location = System::Drawing::Point(786, 59);
			this->previewPanel->Name = L"previewPanel";
			this->previewPanel->Size = System::Drawing::Size(242, 237);
			this->previewPanel->TabIndex = 1;
			// 
			// startButton
			// 
			this->startButton->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->startButton->Location = System::Drawing::Point(89, 276);
			this->startButton->Name = L"startButton";
			this->startButton->Size = System::Drawing::Size(91, 48);
			this->startButton->TabIndex = 2;
			this->startButton->Text = L"Start";
			this->startButton->UseVisualStyleBackColor = true;
			this->startButton->Click += gcnew System::EventHandler(this, &Form1::startButton_Click);
			// 
			// scoreLabel
			// 
			this->scoreLabel->AutoSize = true;
			this->scoreLabel->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 24, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->scoreLabel->ForeColor = System::Drawing::Color::White;
			this->scoreLabel->Location = System::Drawing::Point(57, 129);
			this->scoreLabel->Name = L"scoreLabel";
			this->scoreLabel->Size = System::Drawing::Size(109, 37);
			this->scoreLabel->TabIndex = 3;
			this->scoreLabel->Text = L"Score:";
			// 
			// levelLabel
			// 
			this->levelLabel->AutoSize = true;
			this->levelLabel->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 24, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->levelLabel->ForeColor = System::Drawing::Color::White;
			this->levelLabel->Location = System::Drawing::Point(57, 179);
			this->levelLabel->Name = L"levelLabel";
			this->levelLabel->Size = System::Drawing::Size(100, 37);
			this->levelLabel->TabIndex = 4;
			this->levelLabel->Text = L"Level:";
			// 
			// gameTimer
			// 
			this->gameTimer->Interval = 500;
			this->gameTimer->Tick += gcnew System::EventHandler(this, &Form1::gameTimer_Tick);
			// 
			// scoreCount
			// 
			this->scoreCount->AutoSize = true;
			this->scoreCount->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 24, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->scoreCount->ForeColor = System::Drawing::Color::White;
			this->scoreCount->Location = System::Drawing::Point(156, 129);
			this->scoreCount->Name = L"scoreCount";
			this->scoreCount->Size = System::Drawing::Size(35, 37);
			this->scoreCount->TabIndex = 5;
			this->scoreCount->Text = L"0";
			// 
			// levelCount
			// 
			this->levelCount->AutoSize = true;
			this->levelCount->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 24, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->levelCount->ForeColor = System::Drawing::Color::White;
			this->levelCount->Location = System::Drawing::Point(156, 179);
			this->levelCount->Name = L"levelCount";
			this->levelCount->Size = System::Drawing::Size(33, 37);
			this->levelCount->TabIndex = 6;
			this->levelCount->Text = L"1";
			// 
			// drawTimer
			// 
			this->drawTimer->Interval = 50;
			this->drawTimer->Tick += gcnew System::EventHandler(this, &Form1::drawTimer_Tick);
			// 
			// gameOver
			// 
			this->gameOver->AutoSize = true;
			this->gameOver->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 30, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->gameOver->ForeColor = System::Drawing::Color::White;
			this->gameOver->Location = System::Drawing::Point(12, 59);
			this->gameOver->Name = L"gameOver";
			this->gameOver->Size = System::Drawing::Size(224, 46);
			this->gameOver->TabIndex = 7;
			this->gameOver->Text = L"Game Over";
			this->gameOver->Visible = false;
			// 
			// pictureBox1
			// 
			this->pictureBox1->BackgroundImageLayout = System::Windows::Forms::ImageLayout::None;
			this->pictureBox1->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"pictureBox1.Image")));
			this->pictureBox1->Location = System::Drawing::Point(725, 381);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(356, 306);
			this->pictureBox1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox1->TabIndex = 8;
			this->pictureBox1->TabStop = false;
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::Color::Black;
			this->ClientSize = System::Drawing::Size(1156, 827);
			this->Controls->Add(this->pictureBox1);
			this->Controls->Add(this->gameOver);
			this->Controls->Add(this->levelCount);
			this->Controls->Add(this->scoreCount);
			this->Controls->Add(this->levelLabel);
			this->Controls->Add(this->scoreLabel);
			this->Controls->Add(this->startButton);
			this->Controls->Add(this->previewPanel);
			this->Controls->Add(this->gamePanel);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"$this.Icon")));
			this->MaximizeBox = false;
			this->Name = L"Form1";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"i";
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &Form1::Form1_Paint);
			this->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &Form1::Form1_KeyDown);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox1))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {
				 gamePanel->Height = N_ROWS * CELL_SIZE;
				 gamePanel->Width = N_COLS * CELL_SIZE;
				 previewPanel->Height = PREVIEW_ROWS * CELL_SIZE;
				 previewPanel->Width = PREVIEW_COLS * CELL_SIZE;
				 //Create the insatance of Random
				 rand = gcnew Random();

				 // Set up the two Bitmap buffera
				 gridImage = gcnew Bitmap(gamePanel->Width, gamePanel->Height);
				 previewImage = gcnew Bitmap(previewPanel->Width, previewPanel->Height); 

				 //Get the two graphics objects for the bitmap buffers
				 gridBuffer = Graphics::FromImage(gridImage);
				 previewBuffer = Graphics::FromImage(previewImage);
				 
				 previewBuffer->SmoothingMode = SmoothingMode::AntiAlias;
				 //Get the graphics of both panels
				 panelGraphics = gamePanel->CreateGraphics();
				 previewPanelGraphics = previewPanel->CreateGraphics();

				 formGraphics = CreateGraphics();

				 cellImage = gcnew Bitmap("tetrimino.png");

				 bordersPainted = false;
			 }

	private: System::Void startButton_Click(System::Object^  sender, System::EventArgs^  e) {
				 //Create the new game
				 game = gcnew TetrisGame(gridBuffer, previewBuffer, cellImage ,rand, gameTimer);
				 //Enable the timer
				 gameTimer->Enabled = true;
				 drawTimer->Enabled = true;
				 //Disable the start button
				 startButton->Enabled = false;
				 gameOver->Visible = false;
			 }

	private: System::Void gameTimer_Tick(System::Object^  sender, System::EventArgs^  e) {
				 
				 //Update the game state
				 game->Update();
				 //If the game is still playing then update the score and level
				 if(game->GetIsPlaying())
				 {
					 scoreCount->Text = game->GetScore().ToString();
					 levelCount->Text = game->GetLevel().ToString();
				 }
				 else //The game is now over
				 {
					 //Disable the timer
					 gameTimer->Enabled = false;
					 //Re-enable the start button
					 startButton->Enabled = true;
					 gameOver->Visible = true;
				 }
			 }
	private: System::Void Form1_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e) {
				 game->OnKeyDown(e->KeyCode);
			 }

	private: System::Void drawTimer_Tick(System::Object^  sender, System::EventArgs^  e) {
				 //Clear the buffers
				 gridBuffer->FillRectangle(Brushes::Black, 0,0,gridImage->Width, gridImage->Height);
				 previewBuffer->FillRectangle(Brushes::Black, 0,0,previewImage->Width, previewImage->Height);

				 
				 //Get the game to draw itself to the buffer
				 game->Draw();
				 
				 //Draw the buffer to the panel
				 panelGraphics->DrawImage(gridImage, 0,0, gridImage->Width, gridImage->Height);
				 previewPanelGraphics->DrawImage(previewImage, 0,0, previewImage->Width, previewImage->Height);
			 }
	private: System::Void Form1_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
				 //Only draw the borders once
				 if (!bordersPainted)
				 {
					 PaintBorders(gamePanel);
					 PaintBorders(previewPanel);
					 bordersPainted = true;
				 }
			 }
	private: void PaintBorders(Panel^ p)
			 {
				 Brush^ b = gcnew SolidBrush(Color::DarkSlateGray);
				 int x;
				 int y;
				 int endX;
				 int endY;
				 //Top Side
				 //Get the starting locations for the X and Y
				 x = p->Location.X - CELL_SIZE;
				 y = p->Location.Y - CELL_SIZE;
				 endX = p->Location.X + p->Width;

				 while (x < endX)
				 {
					 formGraphics->FillRectangle(b, x, y, CELL_SIZE, CELL_SIZE);
					 formGraphics->DrawImage(cellImage, x, y, CELL_SIZE, CELL_SIZE);
					 x += CELL_SIZE;
				 }

				 //Right side
				 endY = p->Location.Y + p->Height;
				 while (y < endY)
				 {
					 formGraphics->FillRectangle(b, x, y, CELL_SIZE, CELL_SIZE);
					 formGraphics->DrawImage(cellImage, x, y, CELL_SIZE, CELL_SIZE);
					 y += CELL_SIZE;
				 }

				 //Bottom
				 endX = p->Location.X - CELL_SIZE;

				 while (x > endX)
				 {
					 formGraphics->FillRectangle(b, x, y, CELL_SIZE, CELL_SIZE);
					 formGraphics->DrawImage(cellImage, x, y, CELL_SIZE, CELL_SIZE);
					 x -= CELL_SIZE;
				 }

				 //Left side
				 endY = p->Location.Y - CELL_SIZE;
				  while (y > endY)
				 {
					 formGraphics->FillRectangle(b, x, y, CELL_SIZE, CELL_SIZE);
					 formGraphics->DrawImage(cellImage, x, y, CELL_SIZE, CELL_SIZE);
					 y -= CELL_SIZE;
				 }
			 }
};
}

