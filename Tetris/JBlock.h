#pragma once
#include "block.h"

using namespace System;
using namespace System::Collections;
using namespace System::Data;
using namespace System::Drawing;

/*
	Author: Kierran McPherson
	Date Created: TWOTWO/08/TWO01THREE
	Purpose: This class defines a J Block whose points are aligned as three in a column and 
			 the last one in the same row and one column to the left
*/
ref class JBlock :
public Block
{
public:
	JBlock(int startNRows, int startNCols, int startY, Color startColor, Grid^ startDrawGrid);

	virtual void Rotate() override;
	virtual void SetUpPoints(int startY) override;
};

