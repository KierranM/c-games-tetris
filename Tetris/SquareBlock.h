#pragma once
#include "block.h"

using namespace System;
using namespace System::Collections;
using namespace System::Data;
using namespace System::Drawing;

/*
	Author: Kierran McPherson
	Date Created: 22/08/2013
	Purpose: This class defines a square block which has all four points forming a square

*/
ref class SquareBlock :
public Block
{
public:
	SquareBlock(int startNRows, int startNCols, int startY, Color startColor, Grid^ startDrawGrid);

	virtual void SetUpPoints(int startY) override;
};

