#pragma once
#include "Block.h"
#include "Grid.h"
#include "SquareBlock.h"
#include "LongBlock.h"
#include "TBlock.h"
#include "ZBlock.h"
#include "SBlock.h"
#include "LBlock.h"
#include "JBlock.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

/*
	Author: Kierran McPherson
	Date Created: 25/08/2013
	Purpose: This class defines a game of Tetris. It accepts input from the form and performs the appropriate actions
*/

ref class TetrisGame
{
	public:
#define CELL_SIZE 35
#define N_ROWS 20
#define N_COLS 10
#define PREVIEW_ROWS 7
#define PREVIEW_COLS 7
#define LEVEL_MULTIPLIER 5
#define TIMER_INTERVAL_DECREMENT 20
#define NUMBER_OF_COLOURS 4
#define NUMBER_OF_BLOCK_TYPES 7
#define PREVIEW_START_Y 4
#define BLOCK_START_Y -1

//Combo multipliers
#define SINGLE 40
#define DOUBLE 100
#define TRIPLE 300
#define TETRIS 1200

private:
	Grid^ gameGrid;
	Grid^ previewGrid;
	Block^ liveBlock;
	Block^ nextBlock;
	Random^ rand;
	Timer^ gameTimer;
	bool isPlaying;
	int level;
	int lines;
	int score;

public:
	TetrisGame(Graphics^ startGameGraphics, Graphics^ startPreviewGraphics, Bitmap^ startCellImage, Random^ startRandom, Timer^ timer);

	void Update();
	void Draw();
	void OnKeyDown(Keys key);
	void GameOver();
	Block^ PickRandomBlock(Grid^ grid);
	void LevelUp();
	void SwitchToNextBlock();

	//Gets & Sets
	bool GetIsPlaying()	{return isPlaying;}
	void SetIsPlaying(bool b)	{isPlaying = b;}

	int GetLevel()	{return level;}
	void SetLevel(int lvl)	{level = lvl;}

	int GetScore()	{return score;}
	void SetScore(int s)	{score = s;}
};

