#pragma once
#include "block.h"

using namespace System;
using namespace System::Collections;
using namespace System::Data;
using namespace System::Drawing;

/*
	Author: Kierran McPherson
	Date Created: 22/08/2013
	Purpose: This class defines a S Block which has it's points arranged as two
			 points in a row then another two points one row above and one column to the right
*/

ref class SBlock :
public Block
{
public:
	SBlock(int startNRows, int startNCols, int startY, Color startColor, Grid^ startDrawGrid);

	virtual void Rotate() override;
	virtual void SetUpPoints(int startY) override;
};

