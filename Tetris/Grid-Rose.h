#pragma once
#include "Cell.h"

using namespace System;
using namespace System::Collections;
using namespace System::Data;
using namespace System::Drawing;
using namespace System::Drawing::Drawing2D;

ref class Grid
{
/*
	Author: Kierran McPherson
	Date Created: 21/08/2013
	Purpose: This class defines the Grid of the Tetris game
			 and contains all the information required to draw
			 the grid of Cells to the canvas.
*/
private:
	int nRows;
	int nCols;
	int cellSize;
	array<Cell^, 2>^ cells;
	Bitmap^ cellImage;
	Graphics^ canvas;
public:
	Grid(int startNRows, int startNCols, int size, Bitmap^ startCellImage, Graphics^ startCanvas);

	void DrawGrid();
	void DrawOneCell(Point^ position, Color color);
	bool UpdateGrid();
	bool IsCellFull(Point^ position);
	bool IsRowFull(int row);
	void CopyRowAbove(int row);
	void AddCellToGrid(Point^ position, Color color);
	void Rotate(float angle);

	//Gets & Sets
	int GetNRows()	{return nRows;}
	int GetNCols()	{return nCols;}
};

