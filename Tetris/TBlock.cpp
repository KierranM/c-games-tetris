#include "stdafx.h"
#include "TBlock.h"


TBlock::TBlock(int startNRows, int startNCols, int startY, Color startColor, Grid^ startDrawGrid)
	:Block(startNRows, startNCols, startColor, startDrawGrid)
{
	//Set up the points
	SetUpPoints(startY);
}//End of Constructor

void TBlock::Rotate()
{
	
	//Rotate the blocks points to place it in one of the four possible orientations (N,S,E, or W)

	//Store the current orientation
	Orientation newOrientation = orientation;
	//Get a copy of the cellCoords array
	array<Point>^ temp = GetTempArray();

	switch (orientation)
	{
		//Moving North to East
	case Orientation::NORTH:
		temp[0].X++;
		temp[0].Y--;
		temp[1].X++;
		temp[1].Y++;
		temp[THREE].X--;
		temp[THREE].Y++;
		newOrientation = Orientation::EAST;
		break;
		//Moving East to South
	case Orientation::EAST:
		temp[0].X++;
		temp[0].Y++;
		temp[1].X--;
		temp[1].Y++;
		temp[THREE].X--;
		temp[THREE].Y--;
		newOrientation = Orientation::SOUTH;
		break;
		//Moving South to West
	case Orientation::SOUTH:
		temp[0].X--;
		temp[0].Y++;
		temp[1].X--;
		temp[1].Y--;
		temp[THREE].X++;
		temp[THREE].Y--;
		newOrientation = Orientation::WEST;
		break;
		//Moving West back to North
	case Orientation::WEST:
		temp[0].X--;
		temp[0].Y--;
		temp[1].X++;
		temp[1].Y--;
		temp[THREE].X++;
		temp[THREE].Y++;
		newOrientation = Orientation::NORTH;
		break;
	}//End of SWITCH

	//Check if the rotation is valid
	if (CheckCanMove(temp))
	{
		//Change cellCoords to point at the temp array
		cellCoords = temp;
		//Update the blocks orientation
		orientation = newOrientation;
	}//End of IF
}//End of Rotate

void TBlock::SetUpPoints(int startY)
{
	//Sets up the blocks points into there appropriate positions

	//Get the middle value of the number of columns
	int startX = nCols/TWO;

	//Set up the points to make a T
	cellCoords[0] = Point(startX - 1, startY);
	cellCoords[1] = Point(startX, startY - 1);
	cellCoords[TWO] = Point(startX, startY); //Center point
	cellCoords[THREE] = Point(startX + 1, startY);
}//End of SetUpPoints