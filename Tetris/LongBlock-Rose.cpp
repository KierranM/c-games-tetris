#include "stdafx.h"
#include "LongBlock.h"


LongBlock::LongBlock(int startNRows, int startNCols, int startY, Color startColor, Grid^ startDrawGrid)
	:Block(startNRows, startNCols, startColor, startDrawGrid)
{
	//Set up the points
	SetUpPoints(startY);
}

void LongBlock::Rotate()
{
	//Rotates the long block into one of its two orientations (North or East)

	//Store the current orientation
	Orientation newOrientation = orientation;
	//Get a copy of the cellCoords array
	array<Point^>^ temp = GetTempArray();

	switch (orientation)
	{
		//Moving North to East
	case Orientation::NORTH:
		temp[0]->X -= TWO;
		temp[0]->Y -= TWO;
		temp[1]->X--;
		temp[1]->Y--;
		temp[THREE]->X++;
		temp[THREE]->Y++;
		newOrientation = Orientation::EAST;
		break;
		//Moving East back to North
	case Orientation::EAST:
		temp[0]->X += TWO;
		temp[0]->Y += TWO;
		temp[1]->X++;
		temp[1]->Y++;
		temp[THREE]->X--;
		temp[THREE]->Y--;
		newOrientation = Orientation::NORTH;
		break;
	}

	//Check if the rotation is valid
	if (CheckCanMove(temp))
	{
		//Change cellCoords to point at the temp array
		cellCoords = temp;
		//Update the blocks orientation
		orientation = newOrientation;
	}
}

void LongBlock::SetUpPoints(int startY)
{
	//Sets up the blocks points into there appropriate positions

	//Get the middle of the number of columns
	int startX = nCols/TWO;
	//Set up the blocks in a line
	for (int i = 0; i < cellCoords->Length; i++)
	{
		cellCoords[i] = gcnew Point(startX, startY);
		startY--;
	}
}