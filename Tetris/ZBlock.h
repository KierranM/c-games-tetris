#pragma once
#include "block.h"

using namespace System;
using namespace System::Collections;
using namespace System::Data;
using namespace System::Drawing;

/*
	Author: Kierran McPherson
	Date Created: 22/08/2013
	Purpose: This class defines a Z Block which has it's points arranged as two
			 points in a row then another two points one row down and one column to the right
*/

ref class ZBlock :
public Block
{
public:
	ZBlock(int startNRows, int startNCols, int startY, Color startColor, Grid^ startDrawGrid);

	virtual void Rotate() override;
	virtual void SetUpPoints(int startY) override;
};

