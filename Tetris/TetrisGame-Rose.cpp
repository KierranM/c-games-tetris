#include "stdafx.h"
#include "TetrisGame.h"


TetrisGame::TetrisGame(Graphics^ startGameGraphics, Graphics^ startPreviewGraphics, Bitmap^ startCellImage, Random^ startRandom, Timer^ timer)
{
	gameGrid = gcnew Grid(N_ROWS, N_COLS, CELL_SIZE, startCellImage ,startGameGraphics);
	previewGrid = gcnew Grid(PREVIEW_ROWS, PREVIEW_COLS, CELL_SIZE, startCellImage,startPreviewGraphics);
	level = 1;
	score = 0;
	lines = 0;
	rand = startRandom;
	gameTimer = timer;
	liveBlock = PickRandomBlock(gameGrid);
	nextBlock = PickRandomBlock(previewGrid);
	isPlaying = true;
	//Draw both grids and blocks
	previewGrid->DrawGrid();
	gameGrid->DrawGrid();
	nextBlock->Draw();
	liveBlock->Draw(); 
}

void TetrisGame::Update()
{

	//Check if the live block should stop
	if(liveBlock->ShouldStop())
	{
		//The block should stop so check if the game should end
		if (liveBlock->IsGameOver())
		{
			GameOver();
		}
		else //The block should stop but the game should not end
		{
			liveBlock->Stop();
			SwitchToNextBlock();

			bool rowFull;
			int combo = 0;
			do
			{
				//Check if there was a full row
				rowFull = gameGrid->UpdateGrid();

				//if there was a full row increase the score and check if the game should level up
				if (rowFull)
				{
					combo++;
				}
			}
			while (rowFull); //Repeat until there are no longer any rows full
			
			switch (combo)
			{
				case 1:
					score += (level + 1) * SINGLE;
				break;
				case 2:
					score += (level + 1) * DOUBLE;
				break;
				case 3:
					score += (level + 1) * TRIPLE;
				break;
				case 4:
					score += (level + 1) * TETRIS;
				break;
			}

			//Increase the number of lines for this level
			lines += combo;
			LevelUp();
		}
	}
	else //Move the block down
	{
		//Moves the live block
		liveBlock->MoveDown();

	}
}


void TetrisGame::Draw()
{
	//Draws the game to the screen

	//Draw the game grid
	gameGrid->DrawGrid();

	//Draw the live block
	liveBlock->Draw();

	//Rotate the preview grid
	previewGrid->Rotate(1);
	
	//Draw the preview grid and next block
	previewGrid->DrawGrid();
	nextBlock->Draw();
}

void TetrisGame::OnKeyDown(Keys key)
{
	//Check which key was pushed
	switch (key)
	{
	case Keys::Left:
		liveBlock->MoveLeft();
		break;
	case Keys::Up:
		liveBlock->Rotate();
		break;
	case Keys::Right:
		liveBlock->MoveRight();
		break;
	case Keys::Down:
		Update();
		break;
	}
}

void TetrisGame::GameOver()
{
	//Ends the game
	//Todo: Look into getting the game grid to write GAME OVER using cells
	isPlaying = false;
}

Block^ TetrisGame::PickRandomBlock(Grid^ grid)
{
	//Returns a random tetris block
	Block^ block;

	int nRows = grid->GetNRows();
	int nCols = grid->GetNCols();
	int startY;

	//The different grids have different starting values
	if (grid == gameGrid)
	{
		startY = BLOCK_START_Y;
	}
	else
	{
		startY = PREVIEW_START_Y;
	}

	//Choose a random block type
	int blockType = rand->Next(NUMBER_OF_BLOCK_TYPES);
	switch (blockType)
	{
		case 0:
			block = gcnew SquareBlock(nRows, nCols, startY, Color::Yellow, grid);
			break;
		case 1:
			block = gcnew LongBlock(nRows, nCols, startY, Color::White, grid);
			break;
		case 2:
			block = gcnew TBlock(nRows, nCols, startY, Color::Indigo, grid);
			break;
		case 3:
			block = gcnew LBlock(nRows, nCols, startY, Color::Orange, grid);
			break;
		case 4:
			block = gcnew JBlock(nRows, nCols, startY, Color::Blue, grid);
			break;
		case 5:
			block = gcnew SBlock(nRows, nCols, startY, Color::Green, grid);
			break;
		case 6:
			block = gcnew ZBlock(nRows, nCols, startY, Color::Red, grid);
			break;
	}

	return block;
}

void TetrisGame::LevelUp()
{
	//Checks if the score is high enough to level up, If yes increase the level and decrease the timer interval
	if (lines >= level * LEVEL_MULTIPLIER)
	{
		//Reset the number of lines for this level
		lines = 0;
		level++;
		gameTimer->Interval -= TIMER_INTERVAL_DECREMENT;
	}
}

void TetrisGame::SwitchToNextBlock()
{
	//Make the liveBlock the next block and pick a new nextBlock
	
	//Change the nextBlocks Grid
	nextBlock->SetDrawGrid(gameGrid);
	//Change the nextBlocks nRows and nCols values
	nextBlock->SetNRows(gameGrid->GetNRows());
	nextBlock->SetNCols(gameGrid->GetNCols());
	//Reset the block points so that the block fits into the new grid
	nextBlock->SetUpPoints(-1);
	//Make the live block the next block
	liveBlock = nextBlock;
	
	//Pick another nextBlock
	nextBlock = PickRandomBlock(previewGrid);
}

