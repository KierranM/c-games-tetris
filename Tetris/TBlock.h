#pragma once
#include "block.h"

using namespace System;
using namespace System::Collections;
using namespace System::Data;
using namespace System::Drawing;

/*
	Author: Kierran McPherson
	Date Created: 22/08/2013
	Purpose: This class defines a T Block which has 3 points in a line and a third in the middle but on top
*/

ref class TBlock :
public Block
{
public:
	TBlock(int startNRows, int startNCols, int startY, Color startColor, Grid^ startDrawGrid);

	virtual void Rotate() override;
	virtual void SetUpPoints(int startY) override;
};

