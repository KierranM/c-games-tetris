#pragma once
#include "Grid.h"

using namespace System;
using namespace System::Collections;
using namespace System::Data;
using namespace System::Drawing;

ref class Block
{
	/*
	Author: Kierran McPherson
	Date Created: TWO1/08/TWO01THREE
	Purpose: This class defines a block. The block has a collection of points which represent
			 the cells that it is made of. It can rotate, move down, left, and right, and check
			 if the game is over.
*/

#define TWO 2
#define THREE 3
public: 
	enum class Orientation
	{
		NORTH,
		EAST,
		SOUTH,
		WEST
	};

protected:
	int nRows;
	int nCols;
	array<Point^>^ cellCoords;
	Color color;
	Orientation orientation;
	Grid^ drawGrid;


public:
	Block(int startNRows, int startNCols, Color startColor, Grid^ startDrawGrid);
	
	void Draw();

	void MoveDown();
	void MoveLeft();
	void MoveRight();
	virtual void Rotate();
	virtual void SetUpPoints(int startY);

	bool CheckCanMove(array<Point^>^ pointsToCheck);
	bool CheckBounds(Point^ pointToCheck);
	bool IsGameOver();
	bool ShouldStop();
	void Stop();

	array<Point^>^ GetTempArray();

	//Gets & Sets

	int GetNRows()	{return nRows;}
	void SetNRows(int rows)	{nRows = rows;}

	int GetNCols()	{return nCols;}
	void SetNCols(int columns)	{nCols = columns;}

	Color GetColor()	{return  color;}
	void SetColor(Color c)	{color = c;}

	Orientation GetOrientation()	{return orientation;}
	void SetOrientation(Orientation o)	{orientation = o;}

	Grid^ GetDrawGrid()	{return drawGrid;};
	void SetDrawGrid(Grid^ newDrawGrid)	{drawGrid = newDrawGrid;}
};

