#include "stdafx.h"
#include "Block.h"


Block::Block(int startNRows, int startNCols, Color startColor, Grid^ startDrawGrid)
{
	nRows = startNRows;
	nCols = startNCols;
	color = startColor;
	drawGrid = startDrawGrid;
	cellCoords = gcnew array<Point^>(4);

	//Set the default orientation
	orientation = Orientation::NORTH;
}

void Block::Draw()
{
	//Asks the grid to draw each of the Blocks cells
	for (int i = 0; i < cellCoords->Length; i++)
	{
		//Only draw the cell if it is actually on the grid and not above it
		//This should allow for the nice slow entry onto the screen
		if (cellCoords[i]->Y >= 0)
		{
			drawGrid->DrawOneCell(cellCoords[i], color);
		}

	}
}

void Block::MoveDown()
{
	//Increases all of the blocks cellCoords Y values down by one
	//by taking a copy of the cellCoords array and modifying that before
	//checking if the movement is valid. If it is it writes over the original array
	//with the values in temp

	array<Point^>^ temp = GetTempArray();

	for (int i = 0; i < temp->Length; i++)
	{
		temp[i]->Y++;
	}

	if (CheckCanMove(temp))
	{
		//Change cellCoords to point at the temp array
		cellCoords = temp;
	}

}

void Block::MoveLeft()
{
	//Decreases all of the blocks cellCoords X values by one
	array<Point^>^ temp = GetTempArray();

	for (int i = 0; i < temp->Length; i++)
	{
		temp[i]->X--;
	}

	if (CheckCanMove(temp))
	{
		//Change cellCoords to point at the temp array
		cellCoords = temp;
	}
}

void Block::MoveRight()
{
	//Increases all of the blocks cellCoords X values by one
	array<Point^>^ temp = GetTempArray();

	for (int i = 0; i < temp->Length; i++)
	{
		temp[i]->X++;
	}

	if (CheckCanMove(temp))
	{
		//Change cellCoords to point at the temp array
		cellCoords = temp;
	}
}

void Block::Rotate()
{
	//Rotation that does nothing. The children of this class can implement
	//there own shape specific way of rotating
}

void Block::SetUpPoints(int startY)
{
	//Sets up all the points in the same location (0,0). Each child of this class should implement its own version of this
}

bool Block::CheckCanMove(array<Point^>^ pointsToCheck)
{
	//Checks if any of the points in the given array violate any of the conditions of movement
	/* The conditions are:
	* No point can overlap a full cell in the grid
	* No point's X value can be out of bounds
	* No point's Y value can be higher than the number of columns
	*/
	bool canMove = true;
	for (int i = 0; i < pointsToCheck->Length; i++)
	{
		//Check if the point is within the bounds of the grids X axis
		if (CheckBounds(pointsToCheck[i]))
		{
			canMove = false;
		}
		else
		{
			//Check if the point is off the bottom of the grid
			if (pointsToCheck[i]->Y >= nRows)
			{
				canMove = false;
			}
			else
			{
				//Only check if the cell is full if the point's Y value is greater than or equal to zero
				if(pointsToCheck[i]->Y >= 0)
				{
					//Finally to get here the cell must be within the x axis, and not off the bottom
					//so check if it overlaps a full cell
					if (drawGrid->IsCellFull(pointsToCheck[i]))
					{
						canMove = false;
					}
				}
			}
		}

	}

	return canMove;
}

bool Block::CheckBounds(Point^ pointToCheck)
{
	//Checks if the given point's X value are less than zero or greater than the number of rows
	bool outOfBounds = false;

	//Check if the points x value is less than zero
	if (pointToCheck->X < 0)
	{
		outOfBounds = true;
	}
	else
	{
		//Check if the points x value is greater than or equal to the number of columns
		if (pointToCheck->X >= nCols)
		{
			outOfBounds = true;
		}
	}

	return outOfBounds;
}

bool Block::IsGameOver()
{
	//Called if the block should stop. This method checks if any of the cellCoords Y values are 
	//still negative (off the top of the grid) when the blocks should stop. If any are return true.

	bool gameover = false;

	for (int i = 0; i < cellCoords->Length; i++)
	{
		if (cellCoords[i]->Y < 0)
		{
			gameover = true;
		}
	}

	return gameover;
}

bool Block::ShouldStop()
{
	//Checks if any of the blocks points will overlap a full cell in the grid on the next downward movement
	//Used to tell if the block should become part of the grid

	//Get a copy of the cellCoords array
	array<Point^>^ temp = GetTempArray();

	bool stop = false;

	//Move all the points in the copied array down
	for (int i = 0; i < temp->Length; i++)
	{
		temp[i]->Y++;

		//Check if the point is off the bottom of the grid
		if (temp[i]->Y >= nRows)
		{
			stop = true;
		}
		else //It's not off the bottom so check if it overlaps a full cell on the grid
		{
			//Only check if the cell is full if the point's Y value is greater than or equal to zero
			if(temp[i]->Y >= 0)
			{
				if (drawGrid->IsCellFull(temp[i]))
				{
					stop = true;
				}
			}
		}
	}

	//If any of the points were at the bottom of the grid or overlapped a full cell then this will be true
	return stop;
}

void Block::Stop()
{
	//Asks the grid to add the Blocks cells to the grid
	for (int i = 0; i < cellCoords->Length; i++)
	{
		//Only add the cell if it is actually on the grid
		if (cellCoords[i]->Y >= 0)
		{
			drawGrid->AddCellToGrid(cellCoords[i], color);
		}

	}
}

array<Point^>^ Block::GetTempArray()
{
	//Returns a pointer to an array of Points
	//The returned array is a copy of the cellCoords array
	array<Point^>^ temp = gcnew array<Point^>(4);
	for (int i = 0; i < cellCoords->Length; i++)
	{
		//Take a copy of the values of each point
		temp[i] = gcnew Point(cellCoords[i]->X, cellCoords[i]->Y);
	}

	return temp;
}