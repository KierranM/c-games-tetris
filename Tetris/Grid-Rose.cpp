#include "stdafx.h"
#include "Grid.h"


Grid::Grid(int startNRows, int startNCols, int size, Bitmap^ startCellImage, Graphics^ startCanvas)
{
	nRows = startNRows;
	nCols = startNCols;
	cellSize = size;
	cells = gcnew array<Cell^, 2>(nRows, nCols);
	cellImage = startCellImage;
	//Loop through the rows and columns of the cells array filling it up with new black cells
	for (int row = 0; row < nRows; row++)
	{
		for (int column = 0; column < nCols; column++)
		{
			cells[row, column] = gcnew Cell(Color::Black);
		}
	}
	canvas = startCanvas;
}

void Grid::DrawGrid()
{
	// Loops through each column in each row of the cells array and
	// draws a rectangle to the canvas if the cell is full
	for (int row = 0; row < nRows; row++)
	{
		for (int column = 0; column < nCols; column++)
		{
			// Draw the individual cell
			DrawOneCell(gcnew Point(column,row), cells[row,column]->GetColor());
		}
	}
}

void Grid::DrawOneCell(Point^ position, Color color)
{
	// Draws a single rectangle at the given location with the given color
	Brush^ brush = gcnew SolidBrush(color);
	// Fill a rectangle offset by its position multiplied by the size of each cell
	canvas->FillRectangle(brush, position->X * cellSize, position->Y * cellSize, cellSize, cellSize);

	//if the cell is not black then overlay the cell image
	if (color != Color::Black)
	{
		canvas->DrawImage(cellImage,position->X * cellSize, position->Y * cellSize, cellSize, cellSize);
	}
}

bool Grid::UpdateGrid()
{
	// Loops through each row of the cells array in REVERSE order
	// and checks if the row is full, if it is it deletes the row and
	// moves all the rows above it down one

	// This bool will get set to true if a full row is found
	bool rowFull = false;
	//Loop until the second last row
	for (int row = nRows - 1; row >= 0; row--)
	{
		//Only find one row each time
		if (IsRowFull(row) && !rowFull)
		{
			rowFull = true;
		}

		//After a full row has been found this will run for every row afterwards
		if (rowFull)
		{
			CopyRowAbove(row);
		}
	}

	return rowFull;
}

bool Grid::IsCellFull(Point^ position)
{
	// Checks if the cell at the given position in the cells array is marked as full
	return cells[position->Y, position->X]->GetIsFull();
}

bool Grid::IsRowFull(int row)
{
	// Checks if every cell in the given row of the cells array is marked as full
	int emptyCells = 0;
	// Loop through the cells counting how many cells are empty
	for (int column = 0; column < nCols; column++)
	{
		if (!IsCellFull(gcnew Point(column, row)))
		{
			emptyCells++;
		}
	}

	//If there are no empty cells then return true
	if (emptyCells == 0)
	{
		return true;
	}
	else //There is at least one empty cell
	{
		return false;
	}
}

void Grid::CopyRowAbove(int row)
{
	// Copies all the values of the cells in the row above (lower y index)
	// into the cells of the given row

	for (int column = 0; column < nCols; column++)
	{
		//If the row is not the first row
		if (row > 0)
		{
			cells[row, column]->SetIsFull(cells[row - 1, column]->GetIsFull());
			cells[row, column]->SetColor(cells[row - 1, column]->GetColor());
		}
		else //The row is the first row and so must be treated differently
		{
			cells[row, column]->SetIsFull(false);
			cells[row, column]->SetColor(Color::Black);
		}
	}
}

void Grid::AddCellToGrid(Point^ position, Color color)
{
	//Sets the cell in the given x,y position's color and marks it as full
	cells[position->Y, position->X]->SetIsFull(true);
	cells[position->Y, position->X]->SetColor(color);
}

void Grid::Rotate(float angle)
{
	//Rotate the grids graphics object matrix by the given angle

	//Calculate the midpoint of the graphics object
	float middleX = (nCols * cellSize) / 2;
	float middleY = (nRows * cellSize) / 2;

	//Get the matrix from the graphics object
	Matrix^ canvasMat = canvas->Transform;

	//Rotate the matrix around the midpoint using angle
	canvasMat->RotateAt(angle, PointF(middleX, middleY));

	//Send the matrix back to the canvas
	canvas->Transform = canvasMat;
}