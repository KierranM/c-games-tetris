#include "stdafx.h"
#include "Block.h"


Block::Block(int startNRows, int startNCols, Color startColor, Grid^ startDrawGrid)
{
	nRows = startNRows;
	nCols = startNCols;
	color = startColor;
	drawGrid = startDrawGrid;
	cellCoords = gcnew array<Point>(4);

	//Set the default orientation
	orientation = Orientation::NORTH;
}//End of Constructor

void Block::Draw()
{
	//Asks the grid to draw each of the Blocks cells
	for (int i = 0; i < cellCoords->Length; i++)
	{
		//Only draw the cell if it is actually on the grid and not above it
		//This should allow for the nice slow entry onto the screen
		if (cellCoords[i].Y >= 0)
		{
			drawGrid->DrawOneCell(cellCoords[i], color);
		}//End of IF
	}//End of draw loop
}//End of Draw

void Block::MoveDown()
{
	//Increases all of the blocks cellCoords Y values down by one
	//by taking a copy of the cellCoords array and modifying that before
	//checking if the movement is valid. If it is it writes over the original array
	//with the values in temp

	array<Point>^ temp = GetTempArray();

	//Increase the temporary points Y value
	for (int i = 0; i < temp->Length; i++)
	{
		temp[i].Y++;
	}//End of Y increase loop

	if (CheckCanMove(temp))
	{
		//Change cellCoords to point at the temp array
		cellCoords = temp;
	}//End of IF

}//End of MoveDown

void Block::MoveLeft()
{
	//Decreases all of the blocks cellCoords X values by one
	array<Point>^ temp = GetTempArray();

	//Decrease the temporaary points X value
	for (int i = 0; i < temp->Length; i++)
	{
		temp[i].X--;
	}//End of X decrease loop

	if (CheckCanMove(temp))
	{
		//Change cellCoords to point at the temp array
		cellCoords = temp;
	}//End of IF
}//End of MoveLeft

void Block::MoveRight()
{
	//Increases all of the blocks cellCoords X values by one
	array<Point>^ temp = GetTempArray();

	//Increase the temporary points X value
	for (int i = 0; i < temp->Length; i++)
	{
		temp[i].X++;
	}//End of X increas loop

	if (CheckCanMove(temp))
	{
		//Change cellCoords to point at the temp array
		cellCoords = temp;
	}//End of IF
}//End of MoveRight

void Block::Rotate()
{
	//Rotation that does nothing. The children of this class can implement
	//there own shape specific way of rotating
}//End of Rotate

void Block::SetUpPoints(int startY)
{
	//Sets up all the points in the same location (0,0). Each child of this class should implement its own version of this

	//Loop through creating the points at 0,0
	for (int i = 0; i < cellCoords->Length; i++)
	{
		cellCoords[i] = Point(0,0);
	}//End of cellCoords loop
}//End of SetUpPoints

bool Block::CheckCanMove(array<Point>^ pointsToCheck)
{
	//Checks if any of the points in the given array violate any of the conditions of movement
	/* The conditions are:
	* No point can overlap a full cell in the grid
	* No point's X value can be out of bounds
	* No point's Y value can be higher than the number of columns
	*/
	bool canMove = true;
	for (int i = 0; i < pointsToCheck->Length; i++)
	{
		//Check if the point is within the bounds of the grids X axis
		if (CheckBounds(pointsToCheck[i]))
		{
			canMove = false;
		}//End of checking sides IF
		else
		{
			//Check if the point is off the bottom of the grid
			if (pointsToCheck[i].Y >= nRows)
			{
				canMove = false;
			}//End of checking bottom IF
			else
			{
				//Only check if the cell is full if the point's Y value is greater than or equal to zero
				if(pointsToCheck[i].Y >= 0)
				{
					//Finally to get here the cell must be within the x axis, and not off the bottom
					//so check if it overlaps a full cell
					if (drawGrid->IsCellFull(pointsToCheck[i]))
					{
						canMove = false;
					}//End of IsCellFull IF
				}//End of Y >= 0 IF
			}//End of ELSE
		}//End of ELSE
	}//End of FOR loop

	return canMove;
}//End of CheckCanMove

bool Block::CheckBounds(Point pointToCheck)
{
	//Checks if the given point's X value are less than zero or greater than the number of rows
	bool outOfBounds = false;

	//Check if the points x value is less than zero
	if (pointToCheck.X < 0)
	{
		outOfBounds = true;
	}//End of IF
	else
	{
		//Check if the points x value is greater than or equal to the number of columns
		if (pointToCheck.X >= nCols)
		{
			outOfBounds = true;
		}//End of IF
	}//End of ELSE

	return outOfBounds;
}//End of CheckBounds

bool Block::IsGameOver()
{
	//Called if the block should stop. This method checks if any of the cellCoords Y values are 
	//still negative (off the top of the grid) when the blocks should stop. If any are return true.

	bool gameover = false;

	for (int i = 0; i < cellCoords->Length; i++)
	{
		if (cellCoords[i].Y < 0)
		{
			gameover = true;
		}//End of IF
	}//End of cellCoords loop

	return gameover;
}

bool Block::ShouldStop()
{
	//Checks if any of the blocks points will overlap a full cell in the grid on the next downward movement
	//Used to tell if the block should become part of the grid

	//Get a copy of the cellCoords array
	array<Point>^ temp = GetTempArray();

	bool stop = false;

	//Move all the points in the copied array down
	for (int i = 0; i < temp->Length; i++)
	{
		temp[i].Y++;
	}//End of FOR loop

	//If the block cannot move
	if (!CheckCanMove(temp))
	{
		//Set stop to false
		stop = false;
	}//End of IF

	//If any of the points were at the bottom of the grid or overlapped a full cell then this will be true
	return stop;
}//End of ShouldStop

void Block::Stop()
{
	//Asks the grid to add the Blocks cells to the grid
	for (int i = 0; i < cellCoords->Length; i++)
	{
		//Only add the cell if it is actually on the grid
		if (cellCoords[i].Y >= 0)
		{
			drawGrid->AddCellToGrid(cellCoords[i], color);
		}//End of IF

	}//End of cellCoords loop
}

array<Point>^ Block::GetTempArray()
{
	//Returns a pointer to an array of Points
	//The returned array is a copy of the cellCoords array
	array<Point>^ temp = gcnew array<Point>(4);

	for (int i = 0; i < cellCoords->Length; i++)
	{
		//Take a copy of the values of each point
		temp[i] = Point(cellCoords[i].X, cellCoords[i].Y);
	}//End of cellCoords loop

	return temp;
}//End of GetTempArray