#include "stdafx.h"
#include "SquareBlock.h"


SquareBlock::SquareBlock(int startNRows, int startNCols, int startY, Color startColor, Grid^ startDrawGrid)
	:Block(startNRows, startNCols, startColor, startDrawGrid)
{
	//Set up the points
	SetUpPoints(startY);
}//End of Constructor

void SquareBlock::SetUpPoints(int startY)
{
	//Sets up the points in a square shape
	int leftX = nCols/TWO;
	cellCoords[0] = Point(leftX, startY);
	cellCoords[1] = Point(leftX + 1, startY);
	startY++;
	cellCoords[TWO] = Point(leftX, startY);
	cellCoords[THREE] = Point(leftX + 1, startY);
}//End of SetUpPoints