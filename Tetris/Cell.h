#pragma once
using namespace System::Drawing;
/*
	Author: Kierran McPherson
	Date Created: 21/08/2013
	Purpose: This class defines one of the cells of the grid
			 it holds a color and a boolean indicating whether 
			 the cell is full.
*/
ref class Cell
{

private:
	Color color;
	bool isFull;
	
public:
	Cell(Color startColor);

	//Gets & Sets

	Color GetColor()	{return color;}
	void SetColor(Color c)	{color = c;}

	bool GetIsFull()	{return isFull;}
	void SetIsFull(bool b)	{isFull = b;}
};

