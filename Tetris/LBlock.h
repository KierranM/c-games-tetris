#pragma once
#include "block.h"

using namespace System;
using namespace System::Collections;
using namespace System::Data;
using namespace System::Drawing;

/*
	Author: Kierran McPherson
	Date Created: 22/08/2013
	Purpose: This class defines a L Block whose points are aligned as three in a column and 
			 the last one in the same row and one column to the right
*/

ref class LBlock :
public Block
{
public:
	LBlock(int startNRows, int startNCols, int startY, Color startColor, Grid^ startDrawGrid);

	virtual void Rotate() override;
	virtual void SetUpPoints(int startY) override;
};

